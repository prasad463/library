'use strict';

customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">library documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link" >AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-AppModule-7bf7cc05a560e784a15ec04d8182bf4bc0024468b8ddcfd749606c3ce8dda26d00b0b89229ff05e1c35859b5887f4d62948e418a13175355a3bc0ffdb25e219d"' : 'data-target="#xs-controllers-links-module-AppModule-7bf7cc05a560e784a15ec04d8182bf4bc0024468b8ddcfd749606c3ce8dda26d00b0b89229ff05e1c35859b5887f4d62948e418a13175355a3bc0ffdb25e219d"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-AppModule-7bf7cc05a560e784a15ec04d8182bf4bc0024468b8ddcfd749606c3ce8dda26d00b0b89229ff05e1c35859b5887f4d62948e418a13175355a3bc0ffdb25e219d"' :
                                            'id="xs-controllers-links-module-AppModule-7bf7cc05a560e784a15ec04d8182bf4bc0024468b8ddcfd749606c3ce8dda26d00b0b89229ff05e1c35859b5887f4d62948e418a13175355a3bc0ffdb25e219d"' }>
                                            <li class="link">
                                                <a href="controllers/AppController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-AppModule-7bf7cc05a560e784a15ec04d8182bf4bc0024468b8ddcfd749606c3ce8dda26d00b0b89229ff05e1c35859b5887f4d62948e418a13175355a3bc0ffdb25e219d"' : 'data-target="#xs-injectables-links-module-AppModule-7bf7cc05a560e784a15ec04d8182bf4bc0024468b8ddcfd749606c3ce8dda26d00b0b89229ff05e1c35859b5887f4d62948e418a13175355a3bc0ffdb25e219d"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AppModule-7bf7cc05a560e784a15ec04d8182bf4bc0024468b8ddcfd749606c3ce8dda26d00b0b89229ff05e1c35859b5887f4d62948e418a13175355a3bc0ffdb25e219d"' :
                                        'id="xs-injectables-links-module-AppModule-7bf7cc05a560e784a15ec04d8182bf4bc0024468b8ddcfd749606c3ce8dda26d00b0b89229ff05e1c35859b5887f4d62948e418a13175355a3bc0ffdb25e219d"' }>
                                        <li class="link">
                                            <a href="injectables/AppService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/BooksModule.html" data-type="entity-link" >BooksModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-BooksModule-ceaa9d17a0cfb65b0c8c4a494b9ed4b2ab9910fbdde2376a5f76facd982201974027f3519dae18b4ec4df7e97eb4507c0eed92635d7d98cd8f71b5cb410fcdb7"' : 'data-target="#xs-controllers-links-module-BooksModule-ceaa9d17a0cfb65b0c8c4a494b9ed4b2ab9910fbdde2376a5f76facd982201974027f3519dae18b4ec4df7e97eb4507c0eed92635d7d98cd8f71b5cb410fcdb7"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-BooksModule-ceaa9d17a0cfb65b0c8c4a494b9ed4b2ab9910fbdde2376a5f76facd982201974027f3519dae18b4ec4df7e97eb4507c0eed92635d7d98cd8f71b5cb410fcdb7"' :
                                            'id="xs-controllers-links-module-BooksModule-ceaa9d17a0cfb65b0c8c4a494b9ed4b2ab9910fbdde2376a5f76facd982201974027f3519dae18b4ec4df7e97eb4507c0eed92635d7d98cd8f71b5cb410fcdb7"' }>
                                            <li class="link">
                                                <a href="controllers/BooksController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >BooksController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-BooksModule-ceaa9d17a0cfb65b0c8c4a494b9ed4b2ab9910fbdde2376a5f76facd982201974027f3519dae18b4ec4df7e97eb4507c0eed92635d7d98cd8f71b5cb410fcdb7"' : 'data-target="#xs-injectables-links-module-BooksModule-ceaa9d17a0cfb65b0c8c4a494b9ed4b2ab9910fbdde2376a5f76facd982201974027f3519dae18b4ec4df7e97eb4507c0eed92635d7d98cd8f71b5cb410fcdb7"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-BooksModule-ceaa9d17a0cfb65b0c8c4a494b9ed4b2ab9910fbdde2376a5f76facd982201974027f3519dae18b4ec4df7e97eb4507c0eed92635d7d98cd8f71b5cb410fcdb7"' :
                                        'id="xs-injectables-links-module-BooksModule-ceaa9d17a0cfb65b0c8c4a494b9ed4b2ab9910fbdde2376a5f76facd982201974027f3519dae18b4ec4df7e97eb4507c0eed92635d7d98cd8f71b5cb410fcdb7"' }>
                                        <li class="link">
                                            <a href="injectables/BooksService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >BooksService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/EntriesModule.html" data-type="entity-link" >EntriesModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-EntriesModule-05ea98ae929aef99afe9a9d28baa4ca1efe06c9805c81016c9d50562a5bca9fd3193bb05a2c9f63ae5712c5d64fc517499a0c2518e7ab830d8e373c71166e935"' : 'data-target="#xs-controllers-links-module-EntriesModule-05ea98ae929aef99afe9a9d28baa4ca1efe06c9805c81016c9d50562a5bca9fd3193bb05a2c9f63ae5712c5d64fc517499a0c2518e7ab830d8e373c71166e935"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-EntriesModule-05ea98ae929aef99afe9a9d28baa4ca1efe06c9805c81016c9d50562a5bca9fd3193bb05a2c9f63ae5712c5d64fc517499a0c2518e7ab830d8e373c71166e935"' :
                                            'id="xs-controllers-links-module-EntriesModule-05ea98ae929aef99afe9a9d28baa4ca1efe06c9805c81016c9d50562a5bca9fd3193bb05a2c9f63ae5712c5d64fc517499a0c2518e7ab830d8e373c71166e935"' }>
                                            <li class="link">
                                                <a href="controllers/EntriesController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >EntriesController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-EntriesModule-05ea98ae929aef99afe9a9d28baa4ca1efe06c9805c81016c9d50562a5bca9fd3193bb05a2c9f63ae5712c5d64fc517499a0c2518e7ab830d8e373c71166e935"' : 'data-target="#xs-injectables-links-module-EntriesModule-05ea98ae929aef99afe9a9d28baa4ca1efe06c9805c81016c9d50562a5bca9fd3193bb05a2c9f63ae5712c5d64fc517499a0c2518e7ab830d8e373c71166e935"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-EntriesModule-05ea98ae929aef99afe9a9d28baa4ca1efe06c9805c81016c9d50562a5bca9fd3193bb05a2c9f63ae5712c5d64fc517499a0c2518e7ab830d8e373c71166e935"' :
                                        'id="xs-injectables-links-module-EntriesModule-05ea98ae929aef99afe9a9d28baa4ca1efe06c9805c81016c9d50562a5bca9fd3193bb05a2c9f63ae5712c5d64fc517499a0c2518e7ab830d8e373c71166e935"' }>
                                        <li class="link">
                                            <a href="injectables/EntriesService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >EntriesService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/LoginModule.html" data-type="entity-link" >LoginModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-LoginModule-881628eadaf968e83cdf3e5fed03de4b1b3a9e9558d97db1c486359ef54d892e4c6c82e9f8043b86d76d625a1fadd7f536462960ab0d7ecf717dc94461feafb2"' : 'data-target="#xs-controllers-links-module-LoginModule-881628eadaf968e83cdf3e5fed03de4b1b3a9e9558d97db1c486359ef54d892e4c6c82e9f8043b86d76d625a1fadd7f536462960ab0d7ecf717dc94461feafb2"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-LoginModule-881628eadaf968e83cdf3e5fed03de4b1b3a9e9558d97db1c486359ef54d892e4c6c82e9f8043b86d76d625a1fadd7f536462960ab0d7ecf717dc94461feafb2"' :
                                            'id="xs-controllers-links-module-LoginModule-881628eadaf968e83cdf3e5fed03de4b1b3a9e9558d97db1c486359ef54d892e4c6c82e9f8043b86d76d625a1fadd7f536462960ab0d7ecf717dc94461feafb2"' }>
                                            <li class="link">
                                                <a href="controllers/LoginController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >LoginController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-LoginModule-881628eadaf968e83cdf3e5fed03de4b1b3a9e9558d97db1c486359ef54d892e4c6c82e9f8043b86d76d625a1fadd7f536462960ab0d7ecf717dc94461feafb2"' : 'data-target="#xs-injectables-links-module-LoginModule-881628eadaf968e83cdf3e5fed03de4b1b3a9e9558d97db1c486359ef54d892e4c6c82e9f8043b86d76d625a1fadd7f536462960ab0d7ecf717dc94461feafb2"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-LoginModule-881628eadaf968e83cdf3e5fed03de4b1b3a9e9558d97db1c486359ef54d892e4c6c82e9f8043b86d76d625a1fadd7f536462960ab0d7ecf717dc94461feafb2"' :
                                        'id="xs-injectables-links-module-LoginModule-881628eadaf968e83cdf3e5fed03de4b1b3a9e9558d97db1c486359ef54d892e4c6c82e9f8043b86d76d625a1fadd7f536462960ab0d7ecf717dc94461feafb2"' }>
                                        <li class="link">
                                            <a href="injectables/LoginService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >LoginService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/RegisterModule.html" data-type="entity-link" >RegisterModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-RegisterModule-96e35afda98195f75c9c2f1a403defeec6d0cb0de7ae9b698f6291bfbc4a140f756754986eca200c1f38275c133890608f4080b077ed7a86d7031206bfe875b0"' : 'data-target="#xs-controllers-links-module-RegisterModule-96e35afda98195f75c9c2f1a403defeec6d0cb0de7ae9b698f6291bfbc4a140f756754986eca200c1f38275c133890608f4080b077ed7a86d7031206bfe875b0"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-RegisterModule-96e35afda98195f75c9c2f1a403defeec6d0cb0de7ae9b698f6291bfbc4a140f756754986eca200c1f38275c133890608f4080b077ed7a86d7031206bfe875b0"' :
                                            'id="xs-controllers-links-module-RegisterModule-96e35afda98195f75c9c2f1a403defeec6d0cb0de7ae9b698f6291bfbc4a140f756754986eca200c1f38275c133890608f4080b077ed7a86d7031206bfe875b0"' }>
                                            <li class="link">
                                                <a href="controllers/RegisterController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RegisterController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-RegisterModule-96e35afda98195f75c9c2f1a403defeec6d0cb0de7ae9b698f6291bfbc4a140f756754986eca200c1f38275c133890608f4080b077ed7a86d7031206bfe875b0"' : 'data-target="#xs-injectables-links-module-RegisterModule-96e35afda98195f75c9c2f1a403defeec6d0cb0de7ae9b698f6291bfbc4a140f756754986eca200c1f38275c133890608f4080b077ed7a86d7031206bfe875b0"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-RegisterModule-96e35afda98195f75c9c2f1a403defeec6d0cb0de7ae9b698f6291bfbc4a140f756754986eca200c1f38275c133890608f4080b077ed7a86d7031206bfe875b0"' :
                                        'id="xs-injectables-links-module-RegisterModule-96e35afda98195f75c9c2f1a403defeec6d0cb0de7ae9b698f6291bfbc4a140f756754986eca200c1f38275c133890608f4080b077ed7a86d7031206bfe875b0"' }>
                                        <li class="link">
                                            <a href="injectables/RegisterService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RegisterService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                </ul>
                </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#controllers-links"' :
                                'data-target="#xs-controllers-links"' }>
                                <span class="icon ion-md-swap"></span>
                                <span>Controllers</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="controllers-links"' : 'id="xs-controllers-links"' }>
                                <li class="link">
                                    <a href="controllers/AppController.html" data-type="entity-link" >AppController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/BooksController.html" data-type="entity-link" >BooksController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/EntriesController.html" data-type="entity-link" >EntriesController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/LoginController.html" data-type="entity-link" >LoginController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/RegisterController.html" data-type="entity-link" >RegisterController</a>
                                </li>
                            </ul>
                        </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#entities-links"' :
                                'data-target="#xs-entities-links"' }>
                                <span class="icon ion-ios-apps"></span>
                                <span>Entities</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="entities-links"' : 'id="xs-entities-links"' }>
                                <li class="link">
                                    <a href="entities/Books.html" data-type="entity-link" >Books</a>
                                </li>
                                <li class="link">
                                    <a href="entities/Entries.html" data-type="entity-link" >Entries</a>
                                </li>
                                <li class="link">
                                    <a href="entities/Login.html" data-type="entity-link" >Login</a>
                                </li>
                                <li class="link">
                                    <a href="entities/Register.html" data-type="entity-link" >Register</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/HttpExceptionFilter.html" data-type="entity-link" >HttpExceptionFilter</a>
                            </li>
                            <li class="link">
                                <a href="classes/ValidationExceptionFilter.html" data-type="entity-link" >ValidationExceptionFilter</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/AppService.html" data-type="entity-link" >AppService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/BooksService.html" data-type="entity-link" >BooksService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/EntriesService.html" data-type="entity-link" >EntriesService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/LoginService.html" data-type="entity-link" >LoginService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/RegisterService.html" data-type="entity-link" >RegisterService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ValidationPipe.html" data-type="entity-link" >ValidationPipe</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});