import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Books } from 'src/books/entity/books.entity';
import { Register } from 'src/register/entity/register.entity';
import { Repository } from 'typeorm';
import { Entries } from './entity/entries.entity';
import { Login } from './../login/entity/login.entity';

@Injectable()
export class EntriesService {
  logger: Logger;
  constructor(
    @InjectRepository(Entries)
    private readonly entriesRepository: Repository<Entries>,
    @InjectRepository(Login)
    private readonly loginRepository: Repository<Login>,
    @InjectRepository(Register)
    private readonly registerRepository: Repository<Register>,
    @InjectRepository(Books)
    private readonly booksRepository: Repository<Books>,
  ) {
    this.logger = new Logger(EntriesService.name);
  }

  async add(data: Entries) {
    const getone = await this.registerRepository.findOne({
      name: data.personName,
    });
    if (!getone) {
      throw new HttpException('Your not register user', HttpStatus.BAD_REQUEST);
    } else {
      const getonebook = await this.booksRepository.findOne({
        ISBNnumber: data.ISBNnumber,
      });
      if (!getonebook) {
        throw new HttpException(
          'sorry book is not avaliable in the library',
          HttpStatus.BAD_REQUEST,
        );
      } else {
        const entries = new Entries();
        const gettwo = await this.loginRepository.findOne({
          email: getone.email,
        });

        entries.bookName = getonebook.bookname;
        entries.ISBNnumber = getonebook.ISBNnumber;
        entries.personName = getone.name;
        entries.reviewComments = data.reviewComments;
        entries.URL = data.URL;
        entries.finishingDate = new Date();
        entries.addlogindata(gettwo);
        this.logger.log('Entries Added');
        return await this.entriesRepository.save(entries);
      }
    }
  }
}
