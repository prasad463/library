import { Module } from '@nestjs/common';
import { EntriesController } from './entries.controller';
import { EntriesService } from './entries.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Entries } from './entity/entries.entity';
import { Register } from 'src/register/entity/register.entity';
import { Books } from 'src/books/entity/books.entity';
import { Login } from 'src/login/entity/login.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Entries, Register, Books, Login])],
  controllers: [EntriesController],
  providers: [EntriesService],
})
export class EntriesModule {}
