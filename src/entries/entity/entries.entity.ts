import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsNumber } from 'class-validator';
import { Login } from 'src/login/entity/login.entity';
import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Entries {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @Column()
  @IsString()
  personName: string;

  @ApiProperty()
  @Column()
  @IsString()
  bookName: string;

  @ApiProperty()
  @Column()
  @IsNumber()
  ISBNnumber: number;

  @ApiProperty()
  @Column()
  finishingDate: Date;

  @ApiProperty()
  @Column()
  @IsString()
  reviewComments: string;

  @ApiProperty()
  @Column()
  @IsString()
  URL: string;

  @ManyToMany(() => Login, (login) => login.entries, {
    cascade: true,
    onUpdate: 'CASCADE',
  })
  @JoinTable()
  login: Login[];
  addlogindata(login: Login) {
    if (this.login == null) {
      this.login = new Array<Login>();
    }
    this.login.push(login);
  }
}
