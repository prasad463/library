import { Body, Controller, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { EntriesService } from './entries.service';
import { Entries } from './entity/entries.entity';
import { ValidationPipe } from './../pipes/validation.pipe';

@ApiTags('Entries')
@Controller('entries')
export class EntriesController {
  constructor(private readonly entriesService: EntriesService) {}

  @Post('add')
  async postEntrie(@Body(ValidationPipe) data: Entries) {
    return await this.entriesService.add(data);
  }
}
