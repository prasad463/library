import { Body, Controller, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { RegisterService } from './register.service';
import { Register } from './entity/register.entity';
import { ValidationPipe } from './../pipes/validation.pipe';

@ApiTags('Account')
@Controller('account')
export class RegisterController {
  constructor(private readonly registerService: RegisterService) {}

  @Post('register')
  async add(@Body(ValidationPipe) data: Register) {
    return await this.registerService.add(data);
  }
}
