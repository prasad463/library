import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Login } from './../../login/entity/login.entity';

@Entity({ name: 'register' })
export class Register {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @Column()
  @IsString()
  name: string;

  @ApiProperty()
  @Column()
  @IsString()
  phonenumber: string;

  @ApiProperty()
  @Column()
  @IsString()
  email: string;

  @ApiProperty()
  @Column()
  @IsString()
  password: string;

  @ApiProperty()
  @Column()
  @IsString()
  addressline: string;

  @ApiProperty()
  @Column()
  @IsString()
  state: string;

  @ApiProperty()
  @Column()
  @IsString()
  country: string;

  @ApiProperty()
  @Column()
  @IsString()
  pincode: string;

  @OneToOne(() => Login, (login) => login.register, { cascade: true })
  login: Login;
}
