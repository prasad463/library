import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RegisterController } from './register.controller';
import { RegisterService } from './register.service';
import { Login } from '../login/entity/login.entity';
import { Register } from './entity/register.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Register, Login])],
  controllers: [RegisterController],
  providers: [RegisterService],
})
export class RegisterModule {}
