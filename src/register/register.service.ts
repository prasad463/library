import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Register } from './entity/register.entity';
import { Login } from './../login/entity/login.entity';
// eslint-disable-next-line @typescript-eslint/no-var-requires
const bcrypt = require('bcryptjs');
@Injectable()
export class RegisterService {
  logger: Logger;
  constructor(
    @InjectRepository(Register)
    private readonly registerRepository: Repository<Register>,
    @InjectRepository(Login)
    private readonly loginRepository: Repository<Login>,
  ) {
    this.logger = new Logger(RegisterService.name);
  }

  //register user
  async add(data: Register) {
    const register = new Register();
    const login = new Login();
    register.name = data.name;
    register.email = login.email = data.email;
    register.phonenumber = data.phonenumber;
    register.password = login.password = await bcrypt.hash(data.password, 10);
    register.addressline = data.addressline;
    register.state = data.state;
    register.country = data.country;
    register.pincode = data.pincode;

    register.login = login;
    await this.registerRepository.save(register);
    const user = await this.registerRepository.save(register);
    delete user.password;
    this.logger.log('Registration successfull');
    return user;
  }
}
