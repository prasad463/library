import { HttpException, HttpStatus } from '@nestjs/common';

export class BooksException extends HttpException {
  constructor(error: string, httpstatus: HttpStatus) {
    super(error, httpstatus);
  }
}
