import { HttpException, HttpStatus } from '@nestjs/common';

export class LoginException extends HttpException {
  constructor(error: string, httpstatus: HttpStatus) {
    super(error, httpstatus);
  }
}
