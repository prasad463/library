import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Login } from 'src/login/entity/login.entity';
import { Repository } from 'typeorm';

@Injectable()
export class LoginService {
  logger: Logger;
  constructor(
    @InjectRepository(Login)
    private readonly loginRepository: Repository<Login>,
  ) {
    this.logger = new Logger(LoginService.name);
  }

  async login(data: Login) {
    return this.loginRepository.findOne({ email: data.email });
  }

  async user(data: any) {
    return this.loginRepository.findOne(data);
  }
}
