import {
  BadRequestException,
  Body,
  Controller,
  Get,
  HttpStatus,
  Post,
  Req,
  Res,
  UnauthorizedException,
} from '@nestjs/common';
import { LoginService } from './login.service';
import { Login } from './entity/login.entity';
import { ApiTags } from '@nestjs/swagger';
import { ValidationPipe } from './../pipes/validation.pipe';
import { JwtService } from '@nestjs/jwt';
import { Request, Response } from 'express';
import { LoginException } from './../exceptions/login.exception';
// eslint-disable-next-line @typescript-eslint/no-var-requires
const bcrypt = require('bcryptjs');
@ApiTags('Account')
@Controller('account')
export class LoginController {
  constructor(
    private readonly loginService: LoginService,
    private jwtService: JwtService,
  ) {}

  @Post('login')
  async login(
    @Body(ValidationPipe) data: Login,
    @Res({ passthrough: true }) response: Response,
  ) {
    const user = await this.loginService.login(data);
    if (!user) {
      throw new LoginException('invalid user', HttpStatus.NOT_FOUND);
    }

    if (!(await bcrypt.compare(data.password, user.password))) {
      throw new LoginException('invalid password', HttpStatus.NOT_FOUND);
    }
    const jwt = await this.jwtService.signAsync({ id: user.id });

    response.cookie('jwt', jwt, { httpOnly: true });
    return { message: 'success' };
  }

  @Get('user')
  async user(@Req() request: Request) {
    try {
      const cookie = request.cookies['jwt'];
      const data = await this.jwtService.verifyAsync(cookie);
      if (!data) {
        throw new UnauthorizedException();
      }
      const user = await this.loginService.user({ id: data['id'] });
      const { password, ...result } = user;
      return result;
    } catch (e) {
      throw new UnauthorizedException();
    }
  }
  @Post('logout')
  async logout(@Res({ passthrough: true }) response: Response) {
    response.clearCookie('jwt');
    return 'success';
  }
}
