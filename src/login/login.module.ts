import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Login } from 'src/login/entity/login.entity';
import { LoginController } from './login.controller';
import { LoginService } from './login.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Login]),
    JwtModule.register({ secret: 'secret', signOptions: { expiresIn: '1hr' } }),
  ],
  controllers: [LoginController],
  providers: [LoginService],
})
export class LoginModule {}
