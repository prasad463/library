import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import { Entries } from 'src/entries/entity/entries.entity';
import { Register } from 'src/register/entity/register.entity';
import {
  Column,
  Entity,
  JoinColumn,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Login {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @Column()
  @IsString()
  email: string;

  @ApiProperty()
  @Column()
  @IsString()
  password: string;

  @OneToOne(() => Register, (register) => register.login)
  @JoinColumn()
  register: Register;

  @OneToMany(() => Entries, (entries) => entries.login)
  entries: Entries;
}
