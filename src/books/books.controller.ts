import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { BooksService } from './books.service';
import { Books } from './entity/books.entity';
import { ValidationPipe } from './../pipes/validation.pipe';
import { BooksException } from './../exceptions/books.exception';

@ApiTags('Books')
@Controller('books')
export class BooksController {
  constructor(private readonly booksService: BooksService) {}

  @Get()
  async getAll() {
    return await this.booksService.getAll();
  }

  @Get('book/:ISBNnumber')
  async getBook(@Param('ISBNnumber') ISBNnumber: number) {
    return await this.booksService
      .getBook(ISBNnumber)
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new HttpException('Data Not Found', HttpStatus.NOT_FOUND);
        }
      })
      .catch(() => {
        throw new HttpException('Data Not Found', HttpStatus.NOT_FOUND);
      });
  }

  @Post('add')
  async addBook(@Body(ValidationPipe) data: Books) {
    return await this.booksService.add(data);
  }

  @Put('modify/:ISBNnumber')
  async modify(
    @Body(ValidationPipe) newdata: Books,
    @Param('ISBNnumber') ISBNnumber: number,
  ) {
    return this.booksService
      .modify(newdata, ISBNnumber)
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new BooksException('Book Not Found', HttpStatus.NOT_FOUND);
        }
      })
      .catch(() => {
        throw new BooksException('Book Not Found', HttpStatus.NOT_FOUND);
      });
  }

  @Delete('remove/:ISBNnumber')
  async remove(@Param('ISBNnumber') ISBNnumber: number) {
    return this.booksService
      .remove(ISBNnumber)
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new BooksException('Book Not Found', HttpStatus.NOT_FOUND);
        }
      })
      .catch(() => {
        throw new BooksException('Book Not Found', HttpStatus.NOT_FOUND);
      });
  }
}
