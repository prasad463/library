import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Books } from './entity/books.entity';

@Injectable()
export class BooksService {
  logger: Logger;
  constructor(
    @InjectRepository(Books)
    private readonly booksRepository: Repository<Books>,
  ) {
    this.logger = new Logger(BooksService.name);
  }

  async getAll() {
    this.logger.log('Books Fetched');
    return await this.booksRepository.find();
  }

  async getBook(ISBNnumber: number) {
    this.logger.log('Books Fetched by ISBN Number');
    return await this.booksRepository.findOne({ ISBNnumber: ISBNnumber });
  }

  async add(data: Books) {
    await this.booksRepository.save(data);
    this.logger.log('Book Added');
    return 'Book Data added Successfully';
  }

  async modify(info: Books, ISBNnumber: number) {
    const data = await this.booksRepository.findOne({ ISBNnumber: ISBNnumber });
    await this.booksRepository.update({ ISBNnumber: data.ISBNnumber }, info);
    this.logger.log('Book updated');
    return 'Book Data Updated Successfully';
  }

  async remove(ISBNnumber: number) {
    const data = await this.booksRepository.findOne({ ISBNnumber: ISBNnumber });
    await this.booksRepository.delete({ ISBNnumber: data.ISBNnumber });
    this.logger.log('Book deleted');
    return 'Book Data Deleted Successfully';
  }
}
