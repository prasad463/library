import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsString } from 'class-validator';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'books' })
export class Books {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @Column()
  @IsString()
  bookname: string;

  @ApiProperty()
  @Column()
  @IsString()
  bookauthor: string;

  @ApiProperty()
  @Column()
  @IsNumber()
  ISBNnumber: number;

  @ApiProperty()
  @Column()
  @IsNumber()
  noofpages: number;
}
